<?php
session_start();
require 'pdo.php'; 

if (isset($_POST['login'])) {
    $email = $_POST['mail'];
    $password = $_POST['password'];

    $query = "SELECT id, passw FROM user WHERE email = :email";
    $stmt = $pdo->prepare($query);
    $stmt->bindParam(':email', $email);
    $stmt->execute();

    $userData = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($userData && password_verify($password, $userData['passw'])) {
        $_SESSION['user_id'] = $userData['id'];
        header('Location: logged.php'); 
        exit();
    } else {

        header('Location: login.php?error=1'); 
        exit();
    }
}
?>