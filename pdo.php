<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>

<?php
$host= '127.0.0.1';
$user = 'admin';
$pass = 'admin'; 
$db= 'smedia';

$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);

function readAll(){
    global $pdo;
    $req = $pdo->query("SELECT * FROM post ;");
    return $req->fetchAll();
}

function readAllComm(){
    global $pdo;
    $req = $pdo->query("SELECT * FROM commentss ;");
    return $req->fetchAll();
}


function readAllUser(){
    global $pdo;
    $req = $pdo->query("SELECT * FROM user ;");
    return $req->fetchAll();
}

function createPost($txt, $user_id, $post_img) {
    global $pdo;
    $req = $pdo->prepare("INSERT INTO post(txt, user_id, post_img) VALUES(?, ?, ?);");
    $req->execute([$txt, $user_id, $post_img]);
}

 function createComment($comm, $user_id, $post_id) {
    global $pdo;
    $req = $pdo->prepare("INSERT INTO commentss(comm, user_id, post_id) VALUES(?, ?, ?);");
    $req->execute([$comm, $user_id, $post_id]);
}

 function createSignup($username, $email, $passw, $img){
    global $pdo;
    $req = $pdo->prepare("insert into user(username, email,passw, img) values(?,?,?,?);");
    $req->execute([$username, $email, $passw, $img]);
 };

 function getUserById($Id){
    global $pdo;
    $req = $pdo->prepare("select * from user where id=?;");
    $req->execute([$Id]);
    return $req->fetchAll();
}

function deletePost($post_id) {
    global $pdo;
    $stmt = $pdo->prepare("DELETE FROM post WHERE id = ?");
    $stmt->execute([$post_id]);
}

function updateUser($id, $username, $img, $email){
    global $pdo;
    $req = $pdo->prepare('UPDATE user set username=?, img=?,  email=? WHERE id=?;');
    $req->execute([$username, $img, $email,  $id]);
};

function getPostById($post_id) {
    global $pdo;
    $query = "SELECT * FROM post WHERE id = :post_id";
    $stmt = $pdo->prepare($query);
    $stmt->bindParam(':post_id', $post_id, PDO::PARAM_INT);
    $stmt->execute();

    return $stmt->fetch(PDO::FETCH_ASSOC);
}

function getCommentsByPostId($post_id) {
    global $pdo;
    $stmt = $pdo->prepare("SELECT * FROM commentss WHERE post_id = :post_id");
    $stmt->bindParam(':post_id', $post_id, PDO::PARAM_INT);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


function updatePost($post_id, $txt, $user_id, $post_img) {
    global $pdo;
    $req = $pdo->prepare('UPDATE post SET txt=?, post_img=?, user_id=? WHERE id=?');
    $req->execute([$txt, $post_img, $user_id, $post_id]);
}