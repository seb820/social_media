drop database  if exists smedia; 
create database smedia;
use smedia;

create table user(
    id int auto_increment primary key,
    username varchar(255) not null,
    email varchar(255) not null,
    passw varchar(255) not null,
    img varchar(2000) not null
);


create table post(
    id int auto_increment primary key,
    txt varchar(255) not null,
    user_id int,
    post_img varchar(2000) not null,
    post_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    Foreign Key (user_id) REFERENCES user(id) on delete cascade
);
 
CREATE Table likes(
    user_id int,
    post_id int,
    constraint one_user_one_like UNIQUE (user_id, post_id),
    Foreign Key (user_id) REFERENCES user(id) on delete cascade,
    Foreign key (post_id) REFERENCES post(id) on delete cascade
);

create table commentss(
    id int not null auto_increment primary key,
    comm varchar(255) not null,
    user_id int,
    post_id int,
    comment_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE,
    FOREIGN KEY (post_id) REFERENCES post(id) ON DELETE CASCADE
);



DROP USER  if exists 'admin'@'127.0.0.1';
CREATE USER 'admin'@'127.0.0.1' IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON smedia.* TO 'admin'@'127.0.0.1';
