<?php
require "pdo.php";
require "loggedOnly.php"; // Ensure user is logged in

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $post_id = $_POST["post_id"];
    $txt = $_POST["txt"];
    $post_img = $_POST["post_img"];
    $user_id = $_SESSION['user_id'];

    updatePost($post_id, $txt, $user_id, $post_img);
    var_dump($_POST);

    // Redirect back to the edited post
    header("Location: edit.php?post_id=$post_id");
    exit();
} else {
    // Handle other cases if needed
    header('Location: logged.php'); // Redirect to the posts page
    exit();
}
?>
