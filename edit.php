<?php
require "pdo.php";
require "loggedOnly.php"; // Ensure user is logged in

if (isset($_GET['post_id'])) {
    $post_id = $_GET['post_id'];
    $post = getPostById($post_id); // Create this function to retrieve post details

    if (!$post) {
        // Handle invalid post_id here
        header('Location: logged.php'); // Redirect to the posts page
        exit();
    }

    $user_id = $_SESSION['user_id'];

    // Check if the logged-in user is the author of the post
    $isAuthor = ($post['user_id'] == $user_id);

    if (!$isAuthor) {
        // Handle unauthorized access here
        header('Location: logged.php'); // Redirect to the posts page
        exit();
    }
} else {
    // Handle missing post_id here
    header('Location: logged.php'); // Redirect to the posts page
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Post</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <img class="logo" src="letter-s-alphabet-in-brush-style-png.webp">
        <div class="user-info">
            <span class="username"><?= $_SESSION['username'] ?></span>
            <img class="avatar" src="<?= $_SESSION['img'] ?>" alt="User Avatar">
            <a href="logout.php" class="logout-link">Logout</a>
        </div>
    </header>
    <main>
        <div class="profile">
        </div>
        <div class="post">
            <img class="post-image" src="<?= $post['post_img'] ?>" alt="Post Image">
            <p class="post-text"><?= $post['txt'] ?></p>
            <div class="post-icons">
                <a href="update_form.php?post_id=<?= $post['id'] ?>">Update</a>
                <a href="delete_post.php?post_id=<?= $post['id'] ?>">Delete</a>
            </div>
        </div>
    </main>
</body>
</html>