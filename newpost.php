<?php include "loggedOnly.php"; ?>
<link rel="stylesheet" href="style.css">
    
</head>
<body>
    <header>
        <img class="logo" src="" alt="logo">
        <div class="user-info">
            <span class="username"><?= $_SESSION['username'] ?></span>
            <img class="avatar" src="<?= $_SESSION['img'] ?>" alt="User Avatar">
            <a href="logout.php" class="logout-link">Logout</a>
        </div>
    </header>
    <main>
        <form action="create_post.php" method="post">
            <input type="hidden" name="user_id" value="<?= $_SESSION['user_id'] ?>">
            <textarea name="txt" cols="30" rows="10"></textarea>
            <input type="text" name="post_img">
            <input class="button" type="submit" value="Post">
        </form>
    </main>
</body>
</html>