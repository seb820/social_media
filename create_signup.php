<?php include_once "pdo.php";

$username= $_POST["username"];
$email = $_POST["email"];
$passw= $_POST["passw"];
$img = $_POST["img"];

$hashedpassw = password_hash($passw, PASSWORD_DEFAULT);

createSignup($username,$email,$hashedpassw, $img);

header('location:login.php');

?>