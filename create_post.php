<?php
include_once "pdo.php";

if (isset($_POST["txt"]) && isset($_POST["post_img"]) && isset($_POST["user_id"])) {
    $txt = $_POST["txt"];
    $post_img = $_POST["post_img"];
    $user_id = $_POST["user_id"];
    
    createPost($txt, $user_id, $post_img);
    
    header('Location: logged.php');
    exit();
} else {
    echo "Incomplete data"; 
}


?>
