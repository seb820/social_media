

<?php
session_start();
require "pdo.php";

if (isset($_POST['update'])) {
    $id = $_POST['id'];
    $username = $_POST['username'];
    $email = $_POST['email'];
    $img = $_POST['img'];

    // Update user data
    updateUser($id, $username, $email, $img);

    // Update session data if the logged-in user is updating their own profile
    if ($_SESSION['user_id'] == $id) {
        // Fetch the updated user data from the database
        $userData = getUserById($id);
        
        // Update session data with the new values
        $_SESSION['username'] = $userData[0]['username'];
        $_SESSION['img'] = $userData[0]['img'];
    }

    header('Location: logged.php');
    exit();
} else {
    echo "Invalid access"; // Handle invalid access to the script
}
?>
