<?php
session_start(); // Start the session before including other files
include "loggedOnly.php";
include "pdo.php";

$user_id = $_SESSION['user_id'];
$user_data = getUserById($user_id);
?>

<head>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <img class="logo" src="" alt="logo">
        <div class="user-info">
            <span class="username"><?= $_SESSION['username'] ?></span>
            <img class="avatar" src="<?= $_SESSION['img'] ?>" alt="User Avatar">
            <a href="logout.php" class="logout-link">Logout</a>
        </div>
    </header>
    <main>
        <div class="profile">
            <img class="profile-pic" src="<?= $_SESSION['img'] ?>" alt="User Avatar">
            <h1 class="profile-username"><?= $_SESSION['username'] ?></h1>
        </div>

        <div class="edit-profile">
            <h2>Edit Profile</h2>
            <form action="create_profile.php" method="post">

                <input type="hidden" name="id" value="<?= $user_data[0]["id"] ?>">
                
                <label for="username">Username:</label>
                <input type="text" name="username" value="<?= $user_data[0]["username"] ?>">
                
                <label for="email">Change email:</label>
                <input type="text" name="email" value="<?= $user_data[0]["email"] ?>">
                
                <label for="img">Change Avatar:</label>
                <input type="text" name="img" value="<?= $user_data[0]["img"] ?>">
               
                <input id="modif-btn" name="update" type="submit" class="button" value="Update">
            </form>
        </div>
    </main>
</body>
</html>