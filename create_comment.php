<?php

session_start();
include_once "pdo.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $comm = $_POST["comm"];
    $post_id = $_POST["post_id"]; // Retrieve the post_id from the hidden input field

    // Retrieve the user_id from session
    $user_id = $_SESSION["user_id"];

    createComment($comm, $user_id, $post_id);

    header('location:comments.php?post_id=' . $post_id); // Redirect back to the comments page
    exit();
}
?>

