
    <title>logged in</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <img class="logo" src="letter-s-alphabet-in-brush-style-png.webp">
        <div class="user-info">
            <span class="username">John Doe</span>
            <img class="avatar" src="dog.png" alt="User Avatar">
            <a href="login.html" class="logout-link">Logout</a>
        </div>
    </header>
    <div class="comment-container">
        <?php
        require "pdo.php";
        $post_id = $_GET['post_id']; // Get the post ID from the query parameter
      
        $post = getPostById($post_id); // Fetch the post using post ID
        $comments = getCommentsByPostId($post_id); // Fetch comments for the post
        
        ?>
        <div class="profile">
            <img class="profile-pic" src="<?= $post['img'] ?>" alt="User Avatar">
            <h1 class="profile-username"><?= $post['username'] ?></h1>
        </div>
        <div class="post">
            <img class="post-image" src="<?= $post['post_img'] ?>" alt="Post Image">
            <p class="post-text"><?= $post['txt'] ?></p>
        </div>
        <div class="comment-form">
    <form action="create_comment.php" method="post">
        <input type="hidden" name="post_id" value="<?= $post_id ?>">
        <input type="text" name="comm" >
        <input class="button" type="submit" value="Comment">
    </form>
</div>

        <div class="comments-list">
            <h2>Comments:</h2>
            <ul>
                <?php foreach ($comments as $comment): ?>
                    <li><?= $comment['comm'] ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</body>
