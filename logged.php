<?php include "loggedOnly.php"; ?>

<?php
require "pdo.php";
$posts = readAll();
$user_id = $_SESSION['user_id']; // Get the logged-in user's ID
$user_data = getUserById($user_id); // Fetch user data for the logged-in user
?>


    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <img class="logo" src="" alt="logo">
        <div class="user-info">
            <span class="username"><?= $user_data['username'] ?></span>
            <img class="avatar" src="<?= $user_data[0]['img'] ?>" alt="User Avatar">
            <a href="logout.php" class="logout-link">Logout</a>
        </div>
    </header>
    <main>
        <div class="profile">
            <img class="profile-pic" src="<?= $user_data[0]['img'] ?>" alt="User Avatar">
            <h1 class="profile-username"><?= $user_data[0]['username'] ?></h1>
        </div>
       
        <?php foreach ($posts as $post): ?>
        <div class="post">
            <img class="post-image" src="<?= $post['post_img'] ?>" alt="Post Image">
            
            <p class="post-text"><?= $post['txt'] ?></p>
            <div class="post-icons">
            <?php
if ($post['user_id'] == $user_id) { // Show edit button only for user's own posts
    echo '<a href="edit.php?post_id=' . $post['id'] . '"><span class="icon">&#x270E;</span></a>';
}
?>

                <span id="heart" class="icon">&#9825;</span>
                <a href="comments.php?post_id=<?= $post['id'] ?>"> <span class="icon">&#x1F4AC;</span></a>
                <div class="comment-outputs">

                </div>
            </div>
        </div>
        <?php endforeach; ?>

        <div class="new-post">
            <a href="newpost.php">+</a>
        </div>
    </main>
</body>
</html>