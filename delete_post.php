<?php
require "pdo.php";
require "loggedOnly.php"; // Ensure user is logged in

if (isset($_GET['post_id'])) {
    $post_id = $_GET['post_id'];
    $post = getPostById($post_id); // Create this function to retrieve post details

    if (!$post) {
        // Handle invalid post_id here
        header('Location: logged.php'); // Redirect to the posts page
        exit();
    }

    $user_id = $_SESSION['user_id'];

    // Check if the logged-in user is the author of the post
    $isAuthor = ($post['user_id'] == $user_id);

    if (!$isAuthor) {
        // Handle unauthorized access here
        header('Location: logged.php'); // Redirect to the posts page
        exit();
    }

    // Delete post logic
    deletePost($post_id);

    // After deleting, redirect to the posts page
    header('Location: logged.php');
    exit();
} else {
    // Handle missing post_id here
    header('Location: logged.php'); // Redirect to the posts page
    exit();
}
?>
