<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Post</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <img class="logo" src="letter-s-alphabet-in-brush-style-png.webp">
        <div class="user-info">
            <span class="username"><?= $_SESSION['username'] ?></span>
            <img class="avatar" src="<?= $_SESSION['img'] ?>" alt="User Avatar">
            <a href="logout.php" class="logout-link">Logout</a>
        </div>
    </header>
    <main>
        <div class="profile">
        </div>
        <div class="post">
        <form action="update_post.php" method="post">
    <input type="hidden" name="post_id" value="<?= $post['id'] ?>">
    
    <label for="new_txt">New Text:</label>
    <textarea id="new_txt" name="txt" cols="30" rows="5"><?= $post['txt'] ?></textarea>
    
    <label for="new_post_img">New Post Image URL:</label>
    <input type="text" id="post_img" name="post_img" value="<?= $post['post_img'] ?>">
    
    <input class="button" type="submit" value="Update">
</form>
        </div>
    </main>
</body>
</html>
